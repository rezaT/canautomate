#!/usr/bin/env python

from distutils.core import setup

setup(name='CANautomate',
      version='1.0',
      description='Automate CAN message flow for rapid prototyping, research and development',
      author='Reza Taheri',
      author_email='reza.taheri@tuni.fi',
      packages=['canautomate'],
     )