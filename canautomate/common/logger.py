# most basic logger, reset & results can only be called when no function calls log_new_data (beware of threads)
import contextlib
from pathlib import Path
import os
import time
import pandas as pd
from .real_time import RealTimeTicker


class LoggerBasic:
    def __init__(self, get_data_func_handle=None):
        "logs data from a function into self.history, only logs data that was available on the first call to that function, will not add anything that appears later to avoid inconsistency"

        if get_data_func_handle is None:
            raise ValueError(
                "must define a get_data function that returns a dictionary of data for logging"
            )
        self.get_data = get_data_func_handle
        self.reset()

    def reset(self):
        # flag to reset log (or initialize one)
        self.history = None

    def log_new_data(self, *args, additional_data={}):
        try:
            incoming_data = self.get_data()
            if not incoming_data:
                return  # skip if data is not coming yet
        except:
            print("logger could not read data (yet)")
            return

        new_data = {**incoming_data, **additional_data}
        if self.history is None:
            self.history = {}
            for k in new_data:
                self.history[k] = [new_data[k]]
        else:
            for k in self.history.keys():
                if k in new_data:
                    self.history[k].append(new_data[k])


# implementation of a logger that can reset the log safely, even if thread(s) are calling log_new_data continuously. safe means: 1) the data columns will be consistent (not added during reset/write (halted) operations) and 2) logger will not block the threads when logging is halted
class LoggerSafeReset(LoggerBasic):
    def __init__(self, get_data_func_handle=None, verbose=0):
        self._halted = False
        self.verbose = verbose
        super().__init__(get_data_func_handle=get_data_func_handle)

    def log_new_data(self, *args, additional_data={}):
        if self.isLogging():
            super().log_new_data(*args, additional_data=additional_data)

    @contextlib.contextmanager
    def halt_logging(self):  #! important
        self.stop_logging()
        yield
        self.resume_logging()

    def isLogging(self):
        return not self._halted

    def stop_logging(self):
        self._halted = True
        if self.verbose:
            print("logging halted.")

    def resume_logging(self):
        self._halted = False
        if self.verbose:
            print("logging resumed.")

    def reset(self):
        with self.halt_logging():
            super().reset()


# implementation of a logger that can write csv to a file and reset the log "safely"
class LoggerCSVWrite(LoggerSafeReset):
    def __init__(self, get_data_func_handle=None, verbose=0, save_path=None):
        super().__init__(get_data_func_handle, verbose)
        if save_path is None:
            save_path = "./tmp_log_data/"  # specify a folder, data will have timestamps
        self.save_path = Path(save_path)
        os.makedirs(self.save_path, exist_ok=True)

    def save_and_reset(self, file_name_suffix=""):
        # must stop logging here
        with self.halt_logging():
            df = pd.DataFrame(self.history)
            stamp = time.strftime("%d%m_%H%M%S", time.gmtime())
            save_path_dated = (
                self.save_path / f"log_data_{stamp}_{file_name_suffix}.csv"
            )
            if not save_path_dated.exists():
                df.to_csv(save_path_dated, index=False)
            print(f"saved data to {save_path_dated}, logger resets")
        self.reset()


class LoggerRealTime(LoggerCSVWrite):
    def __init__(
        self, get_data_func_handle=None, verbose=0, save_path=None, self_start=False
    ):
        super().__init__(get_data_func_handle, verbose, save_path)
        self.rt_ticker = RealTimeTicker(self_start=self_start)
        self.rt_ticker.set_callbacks([self.log_new_data])

    def log_new_data(self, additional_data={}):
        # since loggerRT runs on its own, we want to keep record of time as additional data. If time is provided in the data with the exact names, it will not be overwritten by the logger
        additional_data = {
            "time.monotonic_ns": time.monotonic_ns(),
            "time.perf_counter": time.perf_counter(),
            "mean_execution_time_logger": self.rt_ticker.mean_execution_time,
            **additional_data,
        }
        return super().log_new_data(additional_data=additional_data)

    def start_logging(self):
        if not self.rt_ticker.is_alive():
            self.rt_ticker.start()
        self.reset()
