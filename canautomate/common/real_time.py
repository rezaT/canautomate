import threading
import time


class RealTimeTicker(threading.Thread):
    """two usages:
    - either run everything in this class's callbacks with self.start = true
    - or manually call RealTimeTicker.tick()
    """

    def __init__(self, dt=0.05, self_start=False, initial_sleep_duration=1):
        super().__init__(daemon=True)
        self.dt = dt
        self.done = False
        self.event = threading.Event()
        self.checkpoint_time = time.perf_counter()
        self.mean_execution_time = 0.0
        self.mean_loop_time = dt
        self.counter = 0
        self.callbacks = []
        self.initial_sleep_duration = initial_sleep_duration
        if self_start:
            time.sleep(self.initial_sleep_duration)
            self.start()

    def set_callbacks(self, callbacks_list):
        if isinstance(callbacks_list, list):
            self.callbacks = callbacks_list
        else:
            raise ValueError("callbacks should be a list")

    def add_callbacks(self, callbacks_list):
        if isinstance(callbacks_list, list):
            self.callbacks += callbacks_list
        else:
            raise ValueError("callbacks should be a list")

    def run(self):
        while not self.done:
            # self.event.wait()
            self.run_callbacks()
            self.event.clear()
            # tick for duration
            self.tick()

    def tick(self):
        elapsed_time = time.perf_counter() - self.checkpoint_time
        execution_time = max(0, elapsed_time)  # time for execution of callbacks
        self.counter += 1
        self.mean_execution_time = self.mean_execution_time + (1 / self.counter) * (
            execution_time - self.mean_execution_time
        )  # record stats using incremental mean
        remaining_time = self.dt - execution_time
        tick_time = max(0, remaining_time)
        time.sleep(tick_time)
        self.checkpoint_time = time.perf_counter()

    def run_callbacks(self):
        for cb in self.callbacks:
            cb()
