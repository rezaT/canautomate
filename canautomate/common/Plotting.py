from plotly.subplots import make_subplots
import plotly.graph_objects as go
import pandas as pd


def plot_multiple_timeseries(
    df,
    x_label,
    selected_cols=None,
    height=1000,  # TODO adjust based on len(selected_cols)
    width=900,
    save_path=None,
    vertical_spacing=0.001,
    show=True,
):
    """plot multiple time series (x_label = 'Time', y = selected_cols), returns a figure handle

    Args:
        df (pd.DataFrame): data table
        x_label (str): label of x axis data, e.g. "Time"
        selected_cols (list): list of y axis labels, there will be one plot for each
        height (int, optional): increase for larger ys. Defaults to 1000.
        width (int, optional): increase for longer data or screens. Defaults to 900.
        save_path (_type_, optional): saves the plot to an html file in this path. Defaults to None.
    """
    if selected_cols is None:
        selected_cols = df.columns  # all values,
    fig = make_subplots(
        rows=len(selected_cols),
        cols=1,
        shared_xaxes=True,
        vertical_spacing=vertical_spacing,
    )

    for i in range(len(selected_cols)):
        col = selected_cols[i]
        p = fig.add_trace(
            go.Scatter(
                x=df[x_label], y=df[col], name=col, hoverlabel=dict(namelength=-1)
            ),
            row=i + 1,
            col=1,
        )
        # p.update_traces(visible='legendonly')
    fig.update_layout(height=height, width=width)
    if show:
        fig.show()
    if save_path is not None:
        fig.write_html(save_path)
    return fig


def compare_two_timeseries(
    df1,
    df2,
    x_label,  # shared x column (if Time, make sure they start at same value)
    selected_cols=None,
    height=1000,  # TODO adjust based on len(selected_cols)
    width=900,
    save_path=None,
    vertical_spacing=0.001,
    show=True,
):
    """plot multiple time series (x_label = 'Time', y = selected_cols), returns a figure handle

    Args:
        df (pd.DataFrame): data table
        x_label (str): label of x axis data, e.g. "Time"
        selected_cols (list): list of y axis labels, there will be one plot for each
        height (int, optional): increase for larger ys. Defaults to 1000.
        width (int, optional): increase for longer data or screens. Defaults to 900.
        save_path (_type_, optional): saves the plot to an html file in this path. Defaults to None.
    """

    if selected_cols is None:
        # selected_cols = list(set(list(df1.columns) + list(df2.columns))) #doesn't preserve order
        selected_cols = list(df1.columns) + [
            col for col in df2.columns if col not in df1.columns
        ]

    fig = make_subplots(
        rows=len(selected_cols),
        cols=1,
        shared_xaxes=True,
        vertical_spacing=vertical_spacing,
    )

    for i in range(len(selected_cols)):
        col = selected_cols[i]
        if col in df1.columns:
            p = fig.add_trace(
                go.Scatter(
                    x=df1[x_label],
                    y=df1[col],
                    name=f"{col}_A",
                    hoverlabel=dict(namelength=-1),
                    opacity=0.8,
                ),
                row=i + 1,
                col=1,
            )
        if col in df2.columns:
            p = fig.add_trace(
                go.Scatter(
                    x=df2[x_label],
                    y=df2[col],
                    name=f"{col}_B",
                    hoverlabel=dict(namelength=-1),
                    opacity=0.8,
                ),
                row=i + 1,
                col=1,
            )
        # p.update_traces(visible='legendonly')
    fig.update_layout(height=height, width=width)
    if show:
        fig.show()
    if save_path is not None:
        fig.write_html(save_path)
    return fig


if __name__ == "__main__":
    N = 100
    timevec = [i for i in range(N)]
    x = [i**2 for i in range(N)]
    y = [0 for i in range(N)]
    df1 = pd.DataFrame({"Time": timevec, "data": x})
    df2 = pd.DataFrame({"Time": timevec, "data": y})
    compare_two_timeseries(
        df1,
        df2,
        x_label="Time",
        height=500,
        width=300,
    )
