from can import Notifier, Message, CanError
from time import perf_counter

class CANBusReader:
    def __init__(self, bus, db, verbose = 1):
        # super(CANBusReader,self).__init__(daemon=True)
        self.bus = bus
        self.db = db
        # self.buffer = can.BufferedReader()
        self.notifier = Notifier(self.bus, [self.store_on_receive])
        self.bus.flush_tx_buffer()
        if verbose:
            print("CAN thread running...")
        self.CANData = {}
        self.currentTimestamp = 0
        self.start_time = perf_counter()

    def get_currentTime(self):
        return perf_counter() - self.start_time

    def store_on_receive(self, msg):
        # msg = self.buffer.get_message()
        self.CANData[msg.arbitration_id] = msg
        self.currentTimestamp = msg.timestamp

    def send_message(self, message): #in case there's a need to send 1x msg irregularly, it is more recommended to use "encore_and_send*" functions
        try:
            self.bus.send(message)
            return True
        except CanError:
            return False

    def flush_buffer(self):
        msg = self.buffer.get_message()
        while msg is not None:
            msg = self.buffer.get_message()

    def cleanup(self):
        print("cleaning up")
        self.notifier.stop()
        self.bus.shutdown()

    def get_last_by_id(self, id):  # read by arbitrationID
        if id in self.CANData:
            msg = self.CANData[id]
        else:
            msg = None
        return msg

    def get_all_data_dict(self): #contains all (encoded data)
        return self.CANData

    def get_all_data(self):  # get decoded data
        all_data = {}
        msgBox = self.get_all_data_dict()
        for id in list(msgBox):
            msg = msgBox[id]
            try:
                all_data = {**all_data, **self.decode_msg(msg)}
            except:
                pass

        return {**{"Time": self.get_currentTime()}, **all_data}

    def decode_msg(self, msg):  
        data = self.db.decode_message(msg.arbitration_id, msg.data)
        return data

    def get_data_by_id_decoded(self, id):
        all_data_dict = self.get_all_data_dict()
        decoded_data = self.decode_msg(all_data_dict[id])
        return decoded_data


    # def run(self): #! legacy manual threading.Thread setup, deprecated implementation 
    #     '''Data reader'''
    #     print('CAN thread running...')
    #     while True: # self.kEvent.wait(timeout=self.RECV_TIME) better than self.kEvent.is_set() + time.sleep(SAMPLE_TIME)
    #         try:
    #             # msg = self.buffer.get_message()
    #             # self.CANData[msg.arbitration_id] = msg
    #             # self.currentTimestamp = msg.timestamp
    #         except:
    #             print('CAN Bus read error...')
    #         if self.callTimeout():
    #             print('timeout exceeded!')
    #             break
    #     self.cleanup()