import threading
from can.broadcastmanager import  ThreadBasedCyclicSendTask
from copy import deepcopy
from can import CanError
from can import Message

class CANWriterCyclic:
    def __init__(self, bus, db):
        self.db = db
        self.bus = bus
        self.tasks_dict = {} # keep track of periodic send tasks by a unique dictionary of CAN_ids and their tasks
        
    def add_send_task(self, CAN_id, initial_data = None, t_send_cycle = 0.05):
        if CAN_id in self.tasks_dict.keys():
            print(f'Cannot set up new task for CAN id: 0x{CAN_id:X}. ID already set up as a task.')
        else:
            msg_template = self.db.get_message_by_frame_id(CAN_id)
            if initial_data is not None:
                encoded_data = msg_template.encode(initial_data)
                msg_template = Message(
                    arbitration_id= CAN_id,
                    data = encoded_data,
                    is_extended_id= msg_template.is_extended_frame
                ) 
            try:
                # self.send_periodic_task = ModifiableCyclicTaskABC(message=msg, period=period) #,100, store_task = False
                send_periodic_task = ThreadBasedCyclicSendTask(
                    bus=self.bus,
                    lock=self.bus._lock_send_periodic,
                    messages=msg_template,
                    period=t_send_cycle,
                )  # ,100, store_task = False
                self.tasks_dict[CAN_id] = send_periodic_task
            except CanError:
                logging.error(f"Send_task setup failed for 0x{CAN_id:X}!")
                return False


    def modify_send_value(self, CAN_id, data):
        if CAN_id not in self.tasks_dict.keys():
            print(f'Cannot modify data for periodic task 0x{CAN_id:X} because there is no task set up')
            return
        msg_template = self.db.get_message_by_frame_id(CAN_id)
        msg_encoder = msg_template.encode
        data_encoded = msg_encoder(data)
        send_periodic_task = self.tasks_dict[CAN_id]
        send_periodic_task.messages[0].data = data_encoded

    def remove_task(self, CAN_id, last_data = None):
        if CAN_id not in self.tasks_dict.keys():
            print(f'Cannot remove periodic task 0x{CAN_id:X}, no task was set up')
        else:
            send_periodic_task = self.tasks_dict.pop(CAN_id) #removes it from the list
            send_periodic_task.stop()
            print(f'removed periodic task 0x{CAN_id:X}')
            if last_data is not None:
                result = self.encode_and_send_by_id(CAN_id, last_data, period=None)
                if result:
                    print(f'set last CAN msg for id: 0x{CAN_id:X}\n')
                else:
                    print(f'failed to set last CAN msg for id: 0x{CAN_id:X}\n')
            
    def delayed_task_remove(self, delay_s, CANID, last_data = None):
        "removes a task after specified time, using a thread (non-blocking). If last_data is specified, ensures that after task removal that data is sent over CAN"
        threading.Timer(delay_s, self.remove_task, args=(CANID, last_data)).start()


    def get_msg_from_name(self, name):
        return self.db.get_message_by_name(name)  # example : name = 'state_1'

    def encode_msg_id_data(self, id, data_decoded):  # dictionary data
        msg_template = self.db.get_message_by_frame_id(id)
        return Message(
            arbitration_id=id,
            data=msg_template.encode(data_decoded),
            is_extended_id=msg_template.is_extended_frame,
            timestamp=self.currentTimestamp,
        )

    def encode_msg_name_data(self, name, data):
        msg_template = self.get_msg_from_name(name)
        return Message(
            arbitration_id=msg_template.frame_id,
            data=msg_template.encode(data),
            is_extended_id=msg_template.is_extended_frame,
            timestamp=self.currentTimestamp,
        )

    def encode_and_send_by_id(self, id, data, period=None):
        msg = self.encode_msg_id_data(id, data)
        periodic = True if period is not None else False
        if not periodic:
            return self.send_message(msg)
        else:
            return self.send_message_periodic(msg, period)

    def encode_and_send_by_name(self, name, data):
        msg = self.encode_msg_name_data(name, data)
        return self.send_message(msg)

    def get_CAN_id_by_name(self, name):
        msg = self.get_msg_from_name(name)
        return msg.frame_id

    def send_message(self, message):
        try:
            self.bus.send(message)
            return True
        except CanError:
            return False