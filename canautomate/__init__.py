from .reader import CANBusReader
from .bus_connect import connect_bus_manual
from .writer import CANWriterCyclic

import os
from pathlib import Path

dir_path = Path(
    os.path.dirname(os.path.abspath(__file__))
)  # use it for relative paths to this directory


class CANAutomate(CANBusReader, CANWriterCyclic):  # use for both read+write
    def __init__(self, *args, **kwargs):
        CANWriterCyclic.__init__(self, *args, **kwargs)
        super().__init__(*args, **kwargs)
