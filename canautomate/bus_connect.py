from can import ThreadSafeBus
from can.interface import Bus as NonTSBus

def connect_bus_manual(bustype = "socketcan", channel = "can0", bitrate = 125000, isThreadSafe = True):
    """Establish a connection to "known" bus type, channel and bitrate

    Args:
        bustype (str, optional): Defaults to "socketcan".
        channel (str, optional): Defaults to "can0".
        bitrate (int, optional): Defaults to 125000.
        isThreadSafe (bool, optional): whether or not to use a ThreadSafe bus from the python-can module. Defaults to True.

    Returns:
        can.interface.Bus : a bus object to read/write messages
    """
    Bus = ThreadSafeBus if isThreadSafe else NonTSBus
    return Bus(bustype = bustype, channel = channel, bitrate = bitrate)
        
        
def connect_bus_automatic(bitrate = 125000, isThreadSafe = True):
    """Loop over known bus types... this is a more general way of connecting across different platforms and connectors, but to be implemented later

    Args:
        bitrate (int, optional): _description_. Defaults to 125000.
        isThreadSafe (bool, optional): _description_. Defaults to True.

    Raises:
        NotImplementedError: _description_
    """
    raise NotImplementedError("")