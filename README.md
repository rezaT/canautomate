

CANAutomate: CAN bus automatic read/write in Python made simple 
==============================
The year is 2023+. You work in an engineering, research & development, or academic environment, and you wonder if there was a [single line solution](#initialize-canautomate) that would fire up a CAN bus connection and let you access the latest CAN messages as *python variables* with their dbc-specified name. You might also like it if you could send frequent commands over the CAN bus, for instance, so your favorite robot can stir your coffee fast and smooth the way you like it. ChatGPT can't help you, but there's good news: now you CAN.  

## Table of Contents
- [CANAutomate: CAN bus automatic read/write in Python made simple](#canautomate-can-bus-automatic-readwrite-in-python-made-simple)
  - [Table of Contents](#table-of-contents)
  - [Getting Started](#getting-started)
    - [(A) Install dependencies](#a-install-dependencies)
    - [(B) Install from this repository](#b-install-from-this-repository)
    - [(B - alternative) install from source](#b---alternative-install-from-source)
  - [Usage](#usage)
    - [(Option 1) Native CAN connection setup](#option-1-native-can-connection-setup)
    - [(Option 2) Virtual CAN connection setup](#option-2-virtual-can-connection-setup)
    - [Python: setup bus and dbc](#python-setup-bus-and-dbc)
    - [Initialize CANAutomate](#initialize-canautomate)
    - [Read the latest data](#read-the-latest-data)
    - [Send data continuously at a fixed rate](#send-data-continuously-at-a-fixed-rate)
  - [Code Tree](#code-tree)
  - [License and Important Safety Notes](#license-and-important-safety-notes)


Getting Started
------------
### (A) Install dependencies
Install python (3.7+) and the required CAN libraries:

    pip install cantools python-can


### (B) Install from this repository
Open a terminal and run the following command

    pip install git+https://gitlab.com/rezaT/canautomate.git

### (B - alternative) install from source
To install from the source codes, navigate to the installation folder of this package after downloading, then install it via:

    pip install -e .

Usage
------------
To use the code in this repo, you need two things: A CAN connection, and a [DBC file](https://www.kvaser.com/developer-blog/an-introduction-j1939-and-dbc-files/). You can follow along the instructions below to setup the CAN bus and work through a [sample DBC file](https://www.autopi.io/blog/the-meaning-of-dbc-file/).
### (Option 1) Native CAN connection setup
First step is to setup the CAN bus. On windows, one needs to install the appropriate drivers. On Linux you may use the standard SocketCAN, for instance:
```bash
    sudo ip link set can0 up type can bitrate 125000
```
### (Option 2) Virtual CAN connection setup
If you don't have real CAN hardware and just want to test the codes, setup a virtual CAN interface instead:
    
```bash
    sudo modprobe vcan
    sudo ip link add dev vcan0 type vcan bitrate 125000
    sudo ip link set up vcan0
```
More details about CAN interface setup can be found [here](https://elinux.org/Bringing_CAN_interface_up)

### Python: setup bus and dbc
We use the `python-can` library for setting up the can bus and `cantools` for dbc files, e.g.:

```python
import cantools
import can
db = cantools.database.load_file( 'examples/sampleDB.dbc')
bus = can.ThreadSafeBus(bustype = 'socketcan', channel = 'vcan0', bitrate = 125000) # virtual can
```
### Initialize CANAutomate
Using CANAutomate is very easy:

```python
from canautomate import CANAutomate
CANAuto = CANAutomate(bus, db) 
```
This spawns a thread in the background to read and organize all incoming data. 
    

### Read the latest data 

All data is redirected to a python dictionary and values are accessible by indexing into it, e.g.:
```python
CANAuto.get_all_data()['DRIVER_HEARTBEAT_cmd']
```
Note that all data entries are overwritten with new data as soon as the CAN messages arrive, old data are not accessible. To send some `DRIVER_HEARTBEAT_cmd` data over the CAN bus to test the above function, open a terminal and run 

    cansend vcan0 064#64 

see [examples/sample_read.py](https://gitlab.com/rezaT/canautomate/-/blob/master/examples/sample_read.py) for full instructions.

### Send data continuously at a fixed rate
 
It's easy to spawn a thread that periodically sends data over the CAN bus.

```python
CAN_id = 224
#! alternatively, use below to get the CAN_id if you want to refer by name (good for extended IDs)
# CAN_id = CANAuto.get_CAN_id_by_name('SG_Test_ID4')
msg_data = {"state_1": 1,
                "state_2": 2,
                "state_3": 10,
                "state_4": 20,
                } # the names should match the descriptions in sampleDB.dbc file  
CANAuto.add_send_task(CAN_id, msg_data, t_send_cycle= 0.01) #100 Hz
```
While the data is flowing at near-constant rate, we can modify the data on-the-fly whenever we like:
```python
msg_data["state_4"] +=  100
CANAuto.modify_send_value(CAN_id, msg_data)
```
The thread can be removed after it's no longer needed
```python
CANAuto.remove_task(CAN_id)
```
We can have as many tasks as we would like to have over each CAN ID. see [examples/send_sinusoidal.py](https://gitlab.com/rezaT/canautomate/-/blob/master/examples/send_sinusoidal.py) for an example of sending sinusoidal inputs over the CAN bus. 


Code Tree
------------

    ├── README.md          <- The high-level overview, installation and usage related to this project.
    ├── canautomate
    │   ├── bus_connect.py      <- Establishes basic CAN bus connection 
    │   ├── reader.py           <- Threaded CAN reader
    │   └── writer.py           <- Threaded fixed-rate CAN writer (set up one task per CAN ID)
    │
    └── examples           <- A set of usage examples of the codes
        ├── sample_read.py      <- Example usage for reading ALL incoming CAN messages (decode by DBC file)
        ├── sample_read.py      <- Example DBC file, from this link (https://www.autopi.io/blog/the-meaning-of-dbc-file/)
        └── send_sinusoidal.py  <- Example usage for setting up fixed-rate SEND tasks to CAN ID, modifying the data (and terminating them)
        
--------

Avatar Photo by [cottonbro studio](https://www.pexels.com/photo/a-person-opening-a-tin-can-6003041/)

## License and Important Safety Notes

By using this software, you agree that you have read and acknowledged the [license](https://gitlab.com/rezaT/canautomate/-/blob/master/LICENSE). This package is intended to speed up research and development, but be mindful that relying on python for real-time and safety-critical use-cases is not advised.  
Author: [Reza Taheri](https://www.linkedin.com/in/abdolreza-taheri/)
