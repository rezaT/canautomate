"""first, setup a virtual CAN connection in linux:
(reference https://python-can.readthedocs.io/en/master/interfaces/socketcan.html)
    
sudo modprobe vcan
sudo ip link add dev vcan0 type vcan bitrate 125000
sudo ip link set vcan0 up
"""
from pathlib import Path
import os
import sys
from cantools.database import load_file as load_db
from canautomate import connect_bus_manual

main_folder_path = Path(os.path.dirname(os.path.abspath(__file__)))
db_path = main_folder_path / "sampleDB.dbc"
db = load_db(db_path)
bus = connect_bus_manual(channel="vcan0")
