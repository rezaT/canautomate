from canautomate.bus_connect import connect_bus_manual
from canautomate.reader import CANBusReader
from cantools.database import load_file as load_db
import time
from load_default_bus_db import bus, db
"""first, setup a virtual CAN connection in linux:
(reference https://python-can.readthedocs.io/en/master/interfaces/socketcan.html)
    
sudo modprobe vcan
sudo ip link add dev vcan0 type vcan bitrate 125000
sudo ip link set vcan0 up
"""

#! setup reader, 
#! you can also use R = CANAutomate(), which has both read & write capabilities
R = CANBusReader(bus, db) 
for i in range(100):
    # msg_data = {"DRIVER_HEARTBEAT_cmd": 10}
    # R.encode_and_send_by_id(100, msg_data)
    # msg_data = {"MOTOR_CMD_steer": 1,
    #             "MOTOR_CMD_drive": 2}
    # R.encode_and_send_by_id(101, msg_data)
    time.sleep(0.5)
    result= {'DRIVER_HEARTBEAT_cmd': R.get_all_data().pop('DRIVER_HEARTBEAT_cmd', None)}
    print(result)
    """
    :: While this is running, open up another linux terminal and run:
    
    cansend vcan0 064#64
    
    :: see if ROS reacts to change in data

    cansend vcan0 064#40
    """
