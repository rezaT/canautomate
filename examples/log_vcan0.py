"read and decode data coming from vcan0, useful for testing LX03IO_AM class before running on actual machine"
from canautomate.common.logger import LoggerRealTime
import time
from canautomate import CANAutomate
from cantools.database import load_file as load_db
from canautomate import connect_bus_manual
from pathlib import Path
import os

#! configure dbc file and bus (bitrate, type, etc.)
main_folder_path = Path(os.path.dirname(os.path.abspath(__file__)))
db_path = main_folder_path / "sampleDB.dbc"
db = load_db(db_path)
bus = connect_bus_manual(bustype="socketcan", channel="vcan0", bitrate=125000)


if __name__ == "__main__":
    vcan0auto = CANAutomate(bus, db)  # initialize the can bus
    logger = LoggerRealTime(
        get_data_func_handle=vcan0auto.get_all_data,
        save_path=main_folder_path / "logged_vcan0/",
    )
    time.sleep(3)  # wait for can thread to get all data
    print("logging started, press ctrl+C to save and exit.")
    logger.start_logging()

    try:
        while True:
            time.sleep(0.03)

    except KeyboardInterrupt:
        logger.save_and_reset()
        time.sleep(0.1)
        print("program exit.")
