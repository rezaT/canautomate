"read and decode data coming from vcan0, useful for testing a model class before running on actual machine"

from load_default_bus_db import db, bus
import time
from canautomate import CANAutomate

if __name__ == "__main__":
    vcan0auto = CANAutomate(bus, db)
    for i in range(1000):
        print(vcan0auto.get_all_data())
        time.sleep(0.3)
