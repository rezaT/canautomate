""" Plot experiment data from a csv file (All signals)
    # ? example usages:
    #  python scripts/plot_experiment_data.py -f tmp/experiment_data/CANtemp/file.csv -ht 5000 -wt 1000 -vs 0.002
    # ? if no file is specified, opens a window to select a .csv file:
    #  python scripts/plot_experiment_data.py    

"""
import argparse
from canautomate.common.Plotting import plot_multiple_timeseries
import pandas as pd
import tkinter
from tkinter import filedialog
import os

root = tkinter.Tk()
root.withdraw()  # use to hide tkinter window


def navigate_file_path():
    currdir = os.getcwd()
    tempdir = filedialog.askopenfilename(
        parent=root, initialdir=currdir, title="Please select a file"
    )
    # if len(tempdir) > 0:
    #     print ("You chose: %s" % tempdir)
    return tempdir


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Plot Experiment Data",
        description="plot all signals in a csv data",
        epilog="",
    )
    parser.add_argument("-f", "--file_path")
    parser.add_argument(
        "-ht", "--height", default="10000"
    )  # can't use -h (reserved for help)
    parser.add_argument("-wt", "--width", default="900")
    parser.add_argument("-vs", "--vertical_spacing", default="0.0009")
    parser.add_argument("-o", "--save_path")
    parser.add_argument("-n", "--no_show", action="store_true")
    args = parser.parse_args()
    if args.file_path is None:
        args.file_path = navigate_file_path()

    df = pd.read_csv(args.file_path)
    plot_multiple_timeseries(
        df=df,
        x_label="Time",
        selected_cols=df.columns,
        height=int(args.height),
        width=int(args.width),
        save_path=args.save_path,
        vertical_spacing=float(args.vertical_spacing),
        show=(not args.no_show),
    )
