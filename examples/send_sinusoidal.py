from canautomate import CANBusReader, CANWriterCyclic
from load_default_bus_db import bus, db
from time import sleep
from math import sin

# may also use CANAutomate(bus, db)
CAN_writer = CANWriterCyclic(bus, db)

CAN_id = 224
msg_data = {
    "state_1": 1,
    "state_2": 2,
    "state_3": 10,
    "state_4": 20,
}
CAN_writer.add_send_task(CAN_id, msg_data, t_send_cycle=0.01)  # 100 Hz

# loop and change the data once in a while
t = 0
for i in range(20):  # watch in CANalyzer or Wireshark or SavvyCAN
    sleep_time = 1  # 0.1
    y = 50 * sin(10 / 6.28 * t)
    msg_data["state_1"] = int(y / 2)
    msg_data["state_2"] = int(y / 3)
    msg_data["state_3"] = int(y)
    msg_data["state_4"] = int(y) + 250
    CAN_writer.modify_send_value(CAN_id, msg_data)
    t += sleep_time
    sleep(sleep_time)
    print(
        f"t:{t}, data: {msg_data}"
    )  # note that the send frame rate is about 100Hz, the data is updated less often
    if i % 5 == 0:  # can kill/ add tasks
        CAN_writer.remove_task(CAN_id)
        print("sleeping")
        sleep(2)
        CAN_writer.add_send_task(CAN_id, msg_data, t_send_cycle=0.01)  # 100 Hz
        print("task set up")
